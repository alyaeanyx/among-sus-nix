{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.among-sus;
  callPackage = path: overrides:
    let f = import path;
    in f ((builtins.intersectAttrs (builtins.functionArgs f) pkgs) // overrides);
in {
  options = {
    services.among-sus = {
      enable = mkEnableOption "among-sus, an Among Us inspired text adventure";
      
      package = mkOption {
        default = callPackage ./among-sus.nix {};
        type = types.package;
      };
      
      port = mkOption {
        default = 1234;
        type = types.port;
      };
    };
  };
  
  config = mkIf cfg.enable {
    systemd.services.among-sus = {
      description = "among-sus text adventure";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        DynamicUser = true;
        ExecStart = "${cfg.package}/bin/among-sus -p ${builtins.toString cfg.port}";
        Restart = "always";
      };
    };
  };
}
