{
  description = "a flake for deploying among-sus to NixOS systems";
  
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
  inputs.src = {
    url = git+https://git.sr.ht/~martijnbraam/among-sus;
    flake = false;
  };
  
  outputs = { self, nixpkgs, src }: {
    defaultPackage.x86_64-linux =
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "among-sus";
        inherit src;
        
        buildInputs = [ gcc ];
        
        installPhase = ''
          mkdir -p $out/bin
          cp among-sus $out/bin
        '';
      };
    
    nixosModule = { config, lib }:
      let 
        cfg = config.services.among-sus;
      in with lib; {
        options = {
          services.among-sus = {
            enable = mkEnableOption "among-sus, an Among Us inspired text adventure";
            
            package = mkOption {
              default = callPackage ./among-sus.nix {};
              type = types.package;
            };
            
            port = mkOption {
              default = 1234;
              type = types.port;
            };
          };
        };
        
        config = mkIf cfg.enable {
          systemd.services.among-sus = {
            description = "among-sus text adventure";
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" ];
            serviceConfig = {
              DynamicUser = true;
              ExecStart = "${cfg.package}/bin/among-sus -p ${builtins.toString cfg.port}";
              Restart = "always";
            };
          };
        };
    };
  };
}