{ pkgs, ... }:

with pkgs; stdenv.mkDerivation {
    name = "among-sus";
    src = fetchgit {
      url = "https://git.sr.ht/~martijnbraam/among-sus";
      rev = "554e60bf52e3fa931661b9414189a92bb8f69d78";
      sha256 = "0j1158nczhvy5i1ykvzvhlv4ndhibgng0dq1lw2bmi8q6k1q1s0w";
    };
    
    buildInputs = [ gcc ];
    
    installPhase = ''
      mkdir -p $out/bin
      cp among-sus $out/bin
    '';
  }
